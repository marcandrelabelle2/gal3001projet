%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programme permettant de: xxxxxxxxxx
% Resolution numerique de l'equation de Fick :
%   dT          ( 1    dT    d2T  )
%   -- = alpha .( - . --- + ----  )
%   dt          ( r    dR    dR2  )
% Conditions aux limites :
% Bout 1 : Centre et Symetrie :
% 0 = k A dT/dr
%
% Bout 2 : Convection :
% h A (T inifini - T surface)= k A dT/dr
% Methode numerique de resolution: elements finis
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Schema de la resolution:%%%%%%%   EXPLICITE  %%%%
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Comparaison entre la methode analytique et methode numerique
% Dans le cadre du cours: Operations Unitaires II
% Date: Hivers 2019
% Professeur: Seddik Khalloufi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% Initialisation de Matlab
clear all
close all
clc
 
% Parametres/Donnees en SI
Ro       = 1000;        % Kg/m3 
Cp       = 4200;        % J/kg K 
K        = 0.5;         % W/mK
alpha    = K/(Ro*Cp);   % m2/s 
L        = 0.4;         % m 
Tinfini  = 100;         % oC
Tini     = 0;           % oC 
h        = 500;         % W/m2K
Time     = 10*3600;       % s 
 
% Choix du nombre de noeuds : peut etre adapte 
n      = 2*10;
% Calcul du pas dans l'espace
dx     = L/(n-1);

% Condition de convergence dt
dt1    = (dx^2)/(2*alpha);
dt2    = (dx^2)/(4*alpha); %dt1>dt2
dt3    = (Ro*Cp*dx^2*(4*L-dx))/((8*L)*(h*dx+alpha));

dt     = 0.99*min(dt2,dt3);

% Condition pour provoquer l'instabilite du systeme (divergence 
% de la solution) . vous pouvez l'essayer pour voir le phenomene mathematique
% dt     = 10*dt;  

% Calcul de certaines constantes 1<i<N et 0<r<R
CST8    = (alpha*dt)/dx^2;           %CONST GEN1
CST9    = (alpha*dt)/2*L*dx;         %CONST GEN2
CST10    = CST8 - CST9;               %Tp @ i-1
CST11    = 1 - 4*CST8;                %Tp @ i
CST12    = CST8 + CST9;               %Tp @ i+1

% Calcul de certaines constantes i=1 et r=0
CST13    = 4*CST8;               %Tp @ i=1
CST14    = 1 - 4*CST8;           %Tp @ i=2

% Calcul de certaines constantes refroidissement
CST15    = (8*L*h*dt)/(Ro*Cp*dx*(4*L-dx));       %Tp @ i=infinity
CST16    = (8*L*alpha*dt)/(Ro*Cp*dx^2*(4*L-dx)); %TP @ i=n-1
CST17    = 1-CST16-CST15;                        %TP @ i=n

% Calcul de certaines constantes
CST1    = CST14;
CST2    = CST13;
CST3    = CST11;
CST4    = CST10;
CST5    = CST15;
CST6    = CST16;
CST7    = CST17;

% formation du vecteur distance
X      = 0:dx:L;
% Initialisation 
TempsT = 0;
T      = Tini*ones(1,n);
% Affichage des conditions initiales : 
% pas necessaire mais c'est pour verification
plot(X,T,'-r.');
xlabel('Distance en m');
ylabel('Temperature en oC');
title('Profils des temperatures');
hold on

% Determination de la matrice qui va contenir les resultats 
Ligne   = ceil(Time/dt);
Col     = n;
Res = zeros(Ligne+1,Col+1);
Res(1,1:Col+1)= [TempsT T];

% La boucle de calcul 
ii = 1;
cci =0;    ccj =0;   cck =0;
while TempsT < Time;
    % calcul de la temperature au condition au limite 1 (noeud i = 1)
    Tp(1,1)      = CST1*T(1,1) + CST2*T(1,2);
    % calcul de la Temperature a l'interieur de la matrice (1 < i < n)
    for jj = 2:1:n-1;
        Tp(1,jj) = CST3*T(1,jj) + CST4*(T(1,jj-1) + T(1,jj+1));
    end;
    % calcul de la Temperature au condition au limite 2 (noeud i = n)
    Tp(1,n)      = CST5*T(1,n) + CST6*T(1,n-1) + CST7*Tinfini;
   
    % Affichage graphique des resultats 
    plot(X,Tp,'color',[cci ccj cck]);
    hold on;
    
    % Boucle de changement de couleur: pas necessaire 
      if cci < 0.9 
          cci = cci + 0.01; 
      else
          if ccj  < 0.9
              ccj = ccj + 0.01;
          else
             cck = cck + 0.01;
             cci =0;
             ccj =0;
          end
      end;
         
    % Mise a jour du temps et de la temperature  
    TempsT = TempsT + dt;
    T = Tp;
    Res(ii+1,1:Col+1)=[TempsT T];
    ii = ii + 1;
end;
% Affichage des resultats sous forme de tableau: pas necessaire
printmat(Res,'Resultats')
% 
% 
% Parametres addimentionnels necessaire pour 
% la comparaison avec la methode analytique ;
Biot     = h*L/K;
Xbar    =  X'/L;
Fo      = (alpha/L^2)*Res(1:ii,1);
% % Utilisation de la solution analytique;
% Precision = 10;
% [Texact] = fdX32B10T0(Xbar,Fo,Biot,Precision);
%   
% % Conversion 
% % Retour aux tempertaures en oC
% for jj=1:1:n;
%     Texact(1:ii,jj)  = Tinfini*Texact(1:ii,jj);
% end
% Retour aux temps en s
Tempstime= (L^2/alpha)*Fo;
% 
% Methodes: Numerique
figure
xlabel('Temps (s)');
ylabel('Temperature (oC)');
title('Methode numerique ');
hold on ;
plot(Tempstime,Res(1:ii,2),'-b');
plot(Tempstime,Res(1:ii,3),'-b');
plot(Tempstime,Res(1:ii,4),'-b');
legend('Solution Numerique','Location','Best')
 
 
% % Comparaison des deux methodes: Analytique et numerique
% figure
% xlabel('Temps (s)');
% ylabel('Temperature (oC)');
% title('Comparaison des deux methodes: Analytique & Numerique ');
% hold on ;
% plot(Tempstime,Texact(1:ii,1),'r.',Tempstime,Res(1:ii,2),'-b');
% plot(Tempstime,Texact(1:ii,2),'r.',Tempstime,Res(1:ii,3),'-b');
% plot(Tempstime,Texact(1:ii,n/8),'r.',Tempstime,Res(1:ii,n/8+1),'-b');
% plot(Tempstime,Texact(1:ii,n/4),'r.',Tempstime,Res(1:ii,n/4+1),'-b');
% plot(Tempstime,Texact(1:ii,n/2),'r.',Tempstime,Res(1:ii,n/2+1),'-b');
% plot(Tempstime,Texact(1:ii,n),'r.',Tempstime,Res(1:ii,n+1),'-b');
% legend('Solution Analytique','Solution Numerique','Location','Best')