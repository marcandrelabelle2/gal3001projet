% La boucle de calcul 
ii = 1;
cci =0;    ccj =0;   cck =0;
while TempsT < Time;
    % calcul de la temperature au condition au limite 1 (noeud i = 1)
    Tp(1,1)      = CST1*T(1,1) + CST2*T(1,2);
    % calcul de la Temperature a l'interieur de la matrice (1 < i < n)
    for jj = 2:1:n-1;
        Tp(1,jj) = CST3*T(1,jj) + CST4*(T(1,jj-1) + T(1,jj+1));
    end;
    % calcul de la Temperature au condition au limite 2 (noeud i = n)
    Tp(1,n)      = CST5*T(1,n) + CST6*T(1,n-1) + CST7*Tinfini;
    % Affichage graphique des resultats 
    plot(X,Tp,'color',[cci ccj cck]);
    hold on;
    % Boucle de changement de couleur: pas necessaire 
      if cci < 0.9 
          cci = cci + 0.01; 
      else
          if ccj  < 0.9
              ccj = ccj + 0.01;
          else
             cck = cck + 0.01;
             cci =0;
             ccj =0;
          end
      end;
         
    % Mise a jour du temps et de la temperature  
    TempsT = TempsT + dt;
    T = Tp;
    Res(ii+1,1:Col+1)=[TempsT T];
    ii = ii + 1;
end;