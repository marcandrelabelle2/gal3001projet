% fdX32B10T0(xd,td,B1,A) function Vector xd and td.
% T_inf = 1 @ xd = 0, convection condition @ xd = 0
% insulated boundary at x=1
% Revision History
% 4 2 12 written by James V. Beck
% 6 21 13 revised by Keith A. Woodbury from fdx31B10T0.m
% calling function: feigXIJ(Mmax,B1,B2) for the X32 eigenvalues
% Variables
% Td = dimensionless temperature, could be a double subscripted array
% xd = dimensionless location, measured from heating surface,
% single value or an array of sizex
% td = dimensionless time, single value or an array of sizet
% B1 = convection coefficient at x=0
% A = desired precision of results to 10^-A accuracy
% used to compute the number of terms needed in the series soln
function Td=fdX32B10T0(xd,td,B1,A)
sizex= max(size(xd)); % start vector option
sizet=max(size(td)); % end vector option
if( sizet > 1 )
dt = td(2)-td(1); % first entry might be zero
else
dt = td(1);
end
xdv=xd; tdv=td;
Mmax=3*floor(1/2+1/pi*sqrt(A*log(10)/dt));
tp=0.48/A;
betar=feigXIJ(Mmax,B1,0); betav=betar(:,2);
for it=1:sizet
td=tdv(it);
for ix=1:sizex
xd=xdv(ix); Md(it,ix)=0; Td(it,ix)=0; %XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
if td == 0
Td(it,ix)=0;
elseif td < tp/4*(2-xd)^2 % short times soln
Td(it,ix) = erfc(xd/sqrt(4*td)) - Ufunc(xd,td,B1);
elseif td < tp/4*(2+xd)^2 % intermediate times soln
Td(it,ix)= erfc(xd/sqrt(4*td)) - Ufunc(xd,td,B1) + ...
erfc((2-xd)/sqrt(4*td)) - Ufunc(2-xd,td,B1);
else
M=floor(1/pi*sqrt(A*log(10)/td));
Md(it,ix)=M;
Td(it,ix)=1; % Start X32B10T0 case.
for m=1:Mmax
beta=betav(m);
Td(it,ix)=Td(it,ix)-B1*2*exp(-beta^2*td)*(beta^2+B1^2)*cos(beta*(1-xd))*cos(beta)/(beta^2+B1^2+B1)/beta^2;
end % for m
end % if
end%ix
end%it
end % function