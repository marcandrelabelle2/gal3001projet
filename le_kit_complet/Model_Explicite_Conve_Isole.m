%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programme permettant de: xxxxxxxxxx
% Resolution numerique de l'equation de Fick :
%   dT          ( 1    dT    d2T  )
%   -- = alpha .( - . --- + ----  )
%   dt          ( r    dR    dR2  )
% Conditions aux limites :
% Bout 1 : Centre et Symetrie :
% 0 = k A dT/dr
%
% Bout 2 : Convection :
% h A (T inifini - T surface)= k A dT/dr
% Methode numerique de resolution: elements finis
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Schema de la resolution:%%%%%%%   EXPLICITE  %%%%
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Comparaison entre la methode numerique explicite 
% et la methode implicite
% Dans le cadre du cours: Operations Unitaires II
% Date: Hivers 2019
% Professeur: Seddik Khalloufi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% Initialisation de Matlab
clear all
close all
clc
 
% Parametres/Donnees en SI
% Source : https://www.researchgate.net/
% ext: /publication/
% 222428478_Thermophysical_properties_of_processed_meat_and_poultry_products
Ro       = 970;         % Kg/m3     970-1070
Cp       = 2850;        % J/kg K    2850-3380
K        = 0.48;        % W/mK      0.26-0.48
alpha    = K/(Ro*Cp);   % m2/s 
Rmax     = 0.1;         % m 
Tinfini  = 80;          % oC
Tini     = 0;           % oC 
h        = 792;         % W/m2K     792-2107
Time     = 2*3600;      % s 

% Choix du nombre de noeuds : peut etre adapte 
n      = 4*10;
% Calcul du pas dans l'espace
dr     = Rmax/(n-1);

% Condition de convergence dt
dt1    = (dr^2)/(2*alpha);
dt2    = (dr^2)/(4*alpha); %dt1>dt2
dt3    = (Ro*Cp*dr^2*(4*Rmax-dr))/((8*Rmax)*(h*dr+alpha));
vdt    = 0.99*[dt1,dt2,dt3];

dt     = min(vdt);

% Constantes generiques
CST9  = (alpha*dt)/(dr)^2;        %CONST GEN1
CST10 = (alpha*dt)/(2*Rmax*dr);   %CONST GEN2

% Calcul de certaines constantes i=1 et r=0
CST1  = 1 - 4*CST9;   %Tp @ i=1
CST2  = 4*CST9;       %Tp @ i=2

% Calcul de certaines constantes 1<i<N et 0<r<R
CST3  = CST9 - CST10;   %Tp @ i-1
CST4  = 1 - 2*CST9;     %Tp @ i
CST5  = CST9 + CST10;   %Tp @ i+1

% Calcul de certaines constantes refroidissement
CST8  = (8*Rmax*h*dt)/(Ro*Cp*dr*(4*Rmax-dr));       %Tp @ i=infinity
CST6  = (8*Rmax*alpha*dt)/(Ro*Cp*dr^2*(4*Rmax-dr)); %TP @ i=n-1
CST7  = 1-CST8-CST6;                                %TP @ i=n

% formation du vecteur distance
R      = 0:dr:Rmax;
% Initialisation 
TempsT = 0;
T      = Tini*ones(1,n);
% Affichage des conditions initiales : pas necessaire mais c'est pour verification
plot(R,T,'-r.');
xlabel('Distance en m');
ylabel('Temperature en oC');
title('Profils des temperatures');
hold on

% Determination de la matrice qui va contenir les resultats 
Ligne   = ceil(Time/dt);
Col     = n;
Res = zeros(Ligne+1,Col+1);
Res(1,1:Col+1)= [TempsT T];


% La boucle de calcul 
ii = 1;
cci =0;    ccj =0;   cck =0;
while TempsT < Time;
    % calcul de la temperature au condition au limite 1 (noeud i = 1)
    Tp(1,1)      = CST1*T(1,1)+ CST2*T(1,2);
    % calcul de la Temperature a l'interieur de la matrice (1 < i < n)
    for jj = 2:1:n-1;
        Tp(1,jj) = CST3*T(1,jj-1) + CST4*T(1,jj) +  CST5*T(1,jj+1);
    end;
    % calcul de la Temperature au condition au limite 2 (noeud i = n)
    Tp(1,n)      = CST6*T(1,n-1) + CST7*T(1,n) + CST8*Tinfini;
   
    % Affichage graphique des resultats 
    plot(R,Tp,'color',[cci ccj cck]);
    hold on;
    
    % Boucle de changement de couleur: pas necessaire 
      if cci < 0.9 
          cci = cci + 0.01; 
      else
          if ccj  < 0.9
              ccj = ccj + 0.01;
          else
             cck = cck + 0.01;
             cci =0;
             ccj =0;
          end
      end;
         
    % Mise a jour du temps et de la temperature  
    TempsT = TempsT + dt;
    T = Tp;
    Res(ii+1,1:Col+1)=[TempsT T];
    ii = ii + 1;
end;
% Affichage des resultats sous forme de tableau: pas necessaire
printmat(Res,'Resultats')

% Parametres addimentionnels necessaire pour 
% la comparaison avec la methode analytique ;
Biot    = h*Rmax/K;
Rbar    =  R/Rmax;
Fo      = (alpha/Rmax^2)*Res(1:ii,1);
 
% % Conversion 
% % Retour aux tempertaures en oC
% for jj=1:1:n;
%     Texact(1:ii,jj)  = Tinfini*Texact(1:ii,jj);
% end
% Retour aux temps en s
Tempstime= (Rmax^2/alpha)*Fo;
 
% Comparaison des deux methodes: Analytique et numerique
figure
xlabel('Temps (s)');
ylabel('Temperature (oC)');
title('Comparaison des deux methodes: Analytique & Numerique ');
hold on ;
plot(Tempstime,Res(1:ii,n),'-b');
plot(Tempstime,Res(1:ii,n-2),'-b');
plot(Tempstime,Res(1:ii,n/2+15),'-b');
plot(Tempstime,Res(1:ii,n/2+10),'-b');
plot(Tempstime,Res(1:ii,n/2+5),'-b');
plot(Tempstime,Res(1:ii,n/2),'-b');
plot(Tempstime,Res(1:ii,n/4),'-b');
legend('Solution Numerique','Location','Best');