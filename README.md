# Projet: Cuisson d'une saucisse

## Table des matieres

* [Rapport du projet en PDF](#rapport-du-projet-en-pdf)
* [Description du projet](#description-du-projet)
* [Modelisation sur MATLAB](#modelisation-sur-matlab)
* [Code source du rapport](#code-source-du-rapport)

## Rapport du projet en PDF

Il est possible de consulter le rapport en cliquant sur le lien suivant :
[RapportDuProjet2019.pdf](RapportPDF/RapportDuProjet2019.pdf)

Pour le télécharger appuyer sur le nuage.

## Description du projet

Le projet a pour but d'apprendre l'tilisation de la méthode 
des éléments finis pour prédire la température 
au sein d’une saucisse lors de la cuisson.

Cet approxmation est fait à l'aide de la méthode de Gauss

## Modelisation sur MATLAB

Voici la liste des fichiers MATLAB : 

* [Model\_Explicite\_Conve\_Isole.m](
le_kit_complet/Model_Explicite_Conve_Isole.m)
* [fdX32B10T0.m](le_kit_complet/fdX32B10T0.m)
* [feigXIJ.m](le_kit_complet/feigXIJ.m)
* [Ufunc.m](le_kit_complet/Ufunc.m)

## Code source du rapport

Le code source du rapport se trouve dans GAL3001-TP.
